import { Component, ChangeDetectorRef } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';
import * as L from 'leaflet';
import 'style-loader!leaflet/dist/leaflet.css';

@Component({
    selector: 'app-page-leaflet',
    templateUrl: './page-leaflet.component.html',
    styleUrls: ['./page-leaflet.component.css']
})
export class PageLeafletComponent {

    public options = {
        layers: [
            L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/dark_nolabels/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
        ],
        zoom: 5,
        center: L.latLng({ lat: 38.991709, lng: -76.886109 }),
    };

    public sidebarVisible: boolean = true;

    constructor(private sidebarService: SidebarService, private cdr: ChangeDetectorRef) {
    }

    toggleFullWidth() {
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
        this.cdr.detectChanges();
    }


}
