export function dateAsDDMMYYYY(date: Date): string {
  const day = date.getDate();
  const month = date.getMonth();
  const year = date.getFullYear();

  return String(day).padStart(2, '0') +
         String(month+1).padStart(2, '0') +
         String(year);
}

export function dateAsYYYYMMDD(date: Date): string {
  const day = date.getDate();
  const month = date.getMonth();
  const year = date.getFullYear();

  return String(year) +
         String(month+1).padStart(2, '0') +
         String(day).padStart(2, '0');
}
