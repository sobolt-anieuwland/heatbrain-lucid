export function flatten(arr: Array<Array<any>>): Array<any> {
    return [].concat.apply([], arr)
}

export function sortNumbers(arr: Array<number>): Array<number> {
    return arr.sort((a, b) => a-b);
}

export function percentile(arr: Array<number>, p: number): number {
    if (arr.length === 0) return 0;
    if (typeof p !== 'number') throw new TypeError('p must be a number');
    if (p <= 0) return arr[0];
    if (p >= 1) return arr[arr.length - 1];

    let index = 0
    if (arr.length % 2 == 1) index = arr.length * p; // odd
    else index = (arr.length - 1) * p

    var lower = Math.floor(index),
        upper = lower + 1,
        weight = index % 1;

    if (upper >= arr.length) return arr[lower];
    return arr[lower] * (1 - weight) + arr[upper] * weight;
}

export function arrayMin(arr: Array<number>): number {
    let len = arr.length,
        min = Infinity;

    while (len--) if (arr[len] < min) min = arr[len];
    return min;
};

export function arrayMax(arr: Array<number>): number {
    let len = arr.length,
        max = -Infinity;

    while (len--) if (arr[len] > max) max = arr[len];
    return max;
};
