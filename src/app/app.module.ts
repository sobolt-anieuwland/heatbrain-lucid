import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { AuthenticateService, SoboltLibModule } from "sobolt-lib";
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from "./store/reducers";
import { HttpClientModule } from "@angular/common/http"

import * as $ from 'jquery';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        routing,
        NgbModule,
        HttpClientModule,
        BrowserAnimationsModule,
        RichTextEditorAllModule,
        LeafletModule.forRoot(),
        SoboltLibModule,
        StoreModule.forRoot(reducers, {
          metaReducers,
          runtimeChecks: {
            strictStateImmutability: true,
            strictActionImmutability: true
          }
        })
    ],
    providers: [AuthenticateService],
    bootstrap: [AppComponent]
})
export class AppModule { }
