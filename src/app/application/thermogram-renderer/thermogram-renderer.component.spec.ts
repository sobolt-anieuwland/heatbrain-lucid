import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThermogramRendererComponent } from './thermogram-renderer.component';

describe('ThermogramRendererComponent', () => {
  let component: ThermogramRendererComponent;
  let fixture: ComponentFixture<ThermogramRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThermogramRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThermogramRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
