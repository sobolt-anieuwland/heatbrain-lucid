import { Input, Output, EventEmitter, Component, OnInit, ChangeDetectorRef, ViewChild, TemplateRef } from '@angular/core';
import { percentile } from '../../functions/arrays'
import { Thermogram } from '../../interfaces/thermogram';
import { HeatLeak } from '../../interfaces/heatleak';
import { FormsModule } from '@angular/forms';
import { CanvasService, DrawPoints } from 'sobolt-lib';
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { PanZoomConfig, PanZoomAPI, PanZoomModel } from "ng2-panzoom";

import * as colormap from 'colormap'

@Component({
  selector: 'app-thermogram-renderer',
  templateUrl: './thermogram-renderer.component.html',
  styleUrls: ['./thermogram-renderer.component.css']
})
export class ThermogramRendererComponent implements OnInit {
  @Input() thermogram: Thermogram
  @Input() classHierarchy: Object | undefined;

  @Input() addingLeak: boolean;
  @Output() addingLeakChange = new EventEmitter<boolean>();
  @Input() editingLeak: boolean;
  @Output() editingLeakChange = new EventEmitter<boolean>();

  heatLeak: HeatLeak | undefined;
  newHeatPoints: DrawPoints[] = []

  private panZoomConfigPanorama: PanZoomConfig = new PanZoomConfig({
    zoomLevels: 10,
    initialZoomLevel: 2.8,
    scalePerZoomLevel: 2.0,
    zoomStepDuration: 0.2,
    freeMouseWheelFactor: 0.01,
    zoomToFitZoomLevelFactor: 0.9,
    dragMouseButton: "left",
    panOnClickDrag: true
  });

  constructor(
    private cdRef: ChangeDetectorRef,
    private canvasService: CanvasService,
    private modalService: NgbModal,
  ) { }

  ngOnInit() { }

  ngOnChange() { }

  ngAfterViewChecked() {
    this.renderThermal()
  }

  public renderThermal() {
    const thermogram = this.thermogram
    const [width, height] = thermogram.getThermalDims()

    this.cdRef.detectChanges()
    const canvas = document.querySelector<HTMLCanvasElement>("canvas")
    canvas.width = width
    canvas.height = height
    const context = canvas.getContext("2d")
    const imgData = context.getImageData(0, 0, width, height)
    const pxlData = imgData.data
    context.fillStyle = "#FF0000"
    context.fillRect(0, 0, width, height)
    const nshades = 255
    const cm = colormap({ colormap: 'jet', nshades: nshades, format: 'rba', alpha: 1 })

    const thermalMin = percentile(thermogram.thermalFlat, thermogram.minPercentile)
    const thermalMax = percentile(thermogram.thermalFlat, thermogram.maxPercentile)

    for (let row = 0; row < height; row++) {
      for (let col = 0; col < width; col++) {
        const canvasOffset = row * width * 4 + col * 4

        const thermalValue = thermogram.thermal[row][col]
        const thermalBounded = Math.min(Math.max(thermalValue, thermalMin),
                                        thermalMax)
        const thermalNormalized = (thermalBounded - thermalMin) / (thermalMax - thermalMin)
        const shadeIdx = Math.floor(thermalNormalized * (nshades-1))

        pxlData[canvasOffset]     = cm[shadeIdx][0]
        pxlData[canvasOffset + 1] = cm[shadeIdx][1]
        pxlData[canvasOffset + 2] = cm[shadeIdx][2]
        pxlData[canvasOffset + 3] = 255 // Transparancy
      }
    }

    context.putImageData(imgData, 0, 0)
  }

  public addPolygonPoints(event: MouseEvent) {
    // Stop processing if not in active state
    if (!this.addingLeak && !this.editingLeak) return;

    // Add new point to collection of points
    console.log("Adding leak point!")
    let x = event.layerX;
    let y = event.layerY;
    this.newHeatPoints.push({x:x, y:y})

    // If we clicked the first point, finalize the annotated leak
    let distance = Math.sqrt(Math.pow((this.newHeatPoints[0].x - x), 2)
                           + Math.pow((this.newHeatPoints[0].y - y), 2))
    if (distance < 6 && this.newHeatPoints.length > 3) {
      this.newHeatPoints.pop();
      let mask = "POLYGON ((";
      for (let idx in this.newHeatPoints) {
        mask += " " + this.newHeatPoints[idx].x.toString()
              + " " + this.newHeatPoints[idx].y.toString()
              + ","
      }
      // Calculate polygon area?
      // https://stackoverflow.com/questions/16285134/calculating-polygon-area
      mask = mask.replace(/.$/,"))")
      let heatleak = {
        'probability': 1,
        'mask': mask,
        'class_code': '0'
      }

      let newHeatLeak = new HeatLeak(heatleak)
      this.thermogram.detectionsKnown.push(newHeatLeak)
      newHeatLeak.hideClassName()

      if (this.editingLeak) {
        newHeatLeak.setClass(this.heatLeak.classCode)
        this.remove()
      }

      this.thermogram.filterHeatLeaks()
      this.cancelManipulation()
      return true;
    }
    return false;
  }

  // Set to inactive state
  public cancelManipulation() {
    this.addingLeak = false
    this.editingLeak = false
    this.newHeatPoints = []
    this.addingLeakChange.emit(this.addingLeak)
    this.editingLeakChange.emit(this.editingLeak)

    for (const hl of this.thermogram.detectionsKnown)
      hl.style = "sobolt-green"
  }

  public showDetails(content, defectIdx) {
    if (this.addingLeak || this.editingLeak) return;

    const size = 'lg'
    this.heatLeak = this.thermogram.detectionsShown[defectIdx]
    this.modalService.open(content, { size: size });
  }

  public handlerRemove(heatLeak: HeatLeak | undefined = undefined) {
    this.modalService.dismissAll()
    this.remove(heatLeak)
    this.thermogram.filterHeatLeaks()
  }

  // Removes the specified heat leak if it is in the set of known detections.
  // If no leak is given, it will remove the currently selected leak, if any.
  public remove(heatLeak: HeatLeak | undefined = undefined) {
    // Get heat leak to remove
    if (heatLeak === undefined) heatLeak = this.heatLeak;
    if (heatLeak !== undefined) this.thermogram.remove(heatLeak);
    this.heatLeak = undefined
  }

  public handlerAdd() {
    this.addingLeak = true
    this.editingLeak = false
    this.modalService.dismissAll()
    this.addingLeakChange.emit(this.addingLeak)

    for (const hl of this.thermogram.detectionsKnown)
      hl.style = "sobolt-green deemphasized"
  }

  public handlerRedraw(heatLeak: HeatLeak) {
    this.addingLeak = false
    this.editingLeak = true
    this.modalService.dismissAll()
    this.editingLeakChange.emit(this.editingLeak)

    for (const hl of this.thermogram.detectionsKnown)
      hl.style = "sobolt-green deemphasized"
  }

  public undo() {
    this.newHeatPoints.pop()
  }
}
