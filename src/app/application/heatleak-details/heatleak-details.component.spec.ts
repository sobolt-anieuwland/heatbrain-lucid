import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeatleakDetailsComponent } from './heatleak-details.component';

describe('HeatleakDetailsComponent', () => {
  let component: HeatleakDetailsComponent;
  let fixture: ComponentFixture<HeatleakDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeatleakDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeatleakDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
