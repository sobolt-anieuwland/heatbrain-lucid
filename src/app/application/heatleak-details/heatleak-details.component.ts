import { Input, Output, EventEmitter, Component, OnInit } from '@angular/core';
import { HeatLeak } from '../../interfaces/heatleak';

@Component({
  selector: 'app-heatleak-details',
  templateUrl: './heatleak-details.component.html',
  styleUrls: ['./heatleak-details.component.css']
})
export class HeatLeakDetailsComponent implements OnInit {
  @Input() heatLeak: HeatLeak | undefined;
  @Input() classHierarchy: Object | undefined;

  @Output() onDeselection = new EventEmitter();
  @Output() onRemove = new EventEmitter<HeatLeak>();
  @Output() onRedraw = new EventEmitter<HeatLeak>();
  @Output() onCancel = new EventEmitter();
  @Output() onUndo = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public handlerClose() {
    this.onDeselection.emit()
  }

  public handlerRemove(event: Event) {
    this.onRemove.emit(this.heatLeak)
  }

  public handlerRedraw(event: Event) {
    this.onRedraw.emit(this.heatLeak)
  }

  public handlerCancelRedraw(event: Event) {
    this.onCancel.emit()
  }

  public handlerUndoRedraw(event: Event) {
    this.onUndo.emit()
  }

  public handlerClassCodeChanged(classCode: string) {
    if (!this.heatLeak) return;
    this.heatLeak.setClass(classCode)
  }
}
