import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeatbrainComponent } from './heatbrain.component';

describe('HeatbrainComponent', () => {
  let component: HeatbrainComponent;
  let fixture: ComponentFixture<HeatbrainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeatbrainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeatbrainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
