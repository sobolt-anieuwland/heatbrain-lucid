// grunt sass om natura style te implementeren

import {
  Component, OnInit, OnChanges, ChangeDetectorRef, ViewChild, TemplateRef
} from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ThermogramRendererComponent } from '../thermogram-renderer/thermogram-renderer.component';
import { ReportService } from '../../services/report.service';
import { JsonSavingService } from '../../services/save_json.service';
import { HeaderBarService } from '../../services/headerbar.service';
import { ReportDataProvider, ReportDataConsumer } from '../../interfaces/report-data-provider';
import { environment } from '../../../environments/environment';
import { Thermogram } from '../../interfaces/thermogram';
import { HeatLeak } from '../../interfaces/heatleak';
import { ReportData } from '../../interfaces/report-data';
import { dateAsYYYYMMDD } from '../../functions/dates';
import { flatten, sortNumbers } from '../../functions/arrays'

@Component({
  selector: 'app-heatbrain',
  templateUrl: './heatbrain.component.html',
  styleUrls: ['./heatbrain.component.css']
})
export class HeatBrainComponent implements OnInit, OnChanges, ReportDataProvider {
  // Thermograms, their heat leaks and the selected / shown thermogram
  thermograms: Array<Thermogram> = [];
  thermogramIdx: number | undefined = undefined

  // Class hierarchy as supplied by the server
  classHierarchy: Object | undefined = undefined

  @ViewChild(ThermogramRendererComponent, {static: false})
  renderer: ThermogramRendererComponent
  addingLeak: boolean = false
  editingLeak: boolean = false
  queryingAIPI: boolean = false

  // Report details dialog variables
  @ViewChild("reportDetails", {static: false}) reportDetailsDialog: TemplateRef<any>
  reportDetailsDialogRef: NgbModalRef | undefined = undefined
  address: string = ""
  city: string = ""
  date: string = String(new Date()).valueOf()
  knmi: string = "260"
  dateEnabled: boolean = false
  lengthReport: string = "short"
  knmiEnabled: boolean = false
  palette: string = "x-rainbow"
  typeReport: string = "Sobolt"

  constructor(
    private http: HttpClient,
    private reporter: ReportService,
    private jsonSaver: JsonSavingService,
    private modalService: NgbModal,
    private headerBar: HeaderBarService,
    private changeDetector: ChangeDetectorRef,
  ) {
    this.reporter.addReportDataProvider(this)
    this.jsonSaver.addReportDataProvider(this)
  }

  ngOnInit() { }

  ngOnChanges() { }

  public provideReportData(consumer: ReportDataConsumer) {
    const self = this

    this.reportDetailsDialogRef = this.modalService.open(this.reportDetailsDialog)
    this.reportDetailsDialogRef.result.then(() => { // TODO implement not generating
      const success = consumer.receiveReportData(new ReportData(
        this.address,
        this.city,
        this.date,
        this.dateEnabled,
        this.lengthReport,
        Number(this.knmi),
        this.knmiEnabled,
        this.palette,
        this.thermograms,
        this.typeReport
      ), () => self.clearResults())
    })

    this.reportDetailsDialogRef.close()

    //this.reportDetailsDialogRef.result.catch(() => { })
  }

  public validInputReportDetailsDialog(
    addressField: HTMLInputElement,
    cityField: HTMLInputElement,
    dateField: HTMLInputElement
  ): boolean {
    return addressField.checkValidity() &&
           cityField.checkValidity() &&
           (dateField.checkValidity() || !this.dateEnabled)
  }

  isModifying() {
    return this.addingLeak || this.editingLeak
  }

  /****************************************************************************/
  /*** File Handling: Reading, Processing, ... ********************************/
  /****************************************************************************/
  /* // Commented out because not necessary anymore - probably
  public handlerFileChosen(event: Event) {
    const element: HTMLInputElement = event.srcElement as HTMLInputElement
    if (element.files.length < 1) return;

    // this.fileCount = element.files.length
    this.clearResults()

    const files: FileList = element.files
    const filesArray: Array<File> = []
    for (let i = 0; i < files.length; i++) {
      const file = files[i]
      filesArray.push(file)
    }

    this.readFiles(filesArray)
  }*/

  handlerFileDropped(files: NgxFileDropEntry[]) {
    if (files.length < 1) return;

    //this.fileCount = event.files.length

    // const file = event.files[0]
    // this.fileName = file.relativePath;
    const filesArray: Array<File> = []
    for (const file of files) {
      if (file.fileEntry.isFile) { // can technically also be a directory
        const fileEntry = file.fileEntry as FileSystemFileEntry;
        fileEntry.file((f: File) => {
          filesArray.push(f);
          if (filesArray.length === files.length)
            this.readFiles(filesArray);
        })
      }
    }
  }

  public readFiles(files: Array<File>) {
    this.clearResults()

    if (files.length === 1 && files[0].type === "application/json") {
      this.readJSON(files[0])
    } else {
      this.readThermograms(files)
    }
  }

  public readJSON(file: File) {
    const fileReader = new FileReader()
    const processJSON = _ => {
      const submission = JSON.parse(fileReader.result as string)

      this.address = submission.address
      this.city = submission.city
      this.palette = submission.palette
      this.typeReport = submission.type_report
      this.lengthReport = submission.length_report

      for (const b64_file of submission.thermograms) {
        const thermogram = new Thermogram(file) // FIXME incorrect file
        thermogram.fileData = b64_file.file

        if (b64_file.hasOwnProperty("beoordelingsgebied")) {
            const bog = new HeatLeak(undefined)
            bog.setClass("Beoordelingsgebied")
            bog.maskXs = b64_file.beoordelingsgebied[0]
            bog.maskYs = b64_file.beoordelingsgebied[1]
            bog.recalculatePolygonCoords()
            bog.hideClassName()
            thermogram.detectionsKnown.push(bog)
        }

        for (const hl_data of b64_file.leaks) {
          const hl = new HeatLeak(undefined)
          thermogram.detectionsKnown.push(hl)
          hl.setClass(hl_data.class)
          hl.maskXs = hl_data.geometry[0]
          hl.maskYs = hl_data.geometry[1]
          hl.recalculatePolygonCoords()
          hl.updateTextBoxPosition()
          hl.hideClassName()
        }

        const endpoint = environment.base_url + "parse-thermogram"
        this.http
            .post(endpoint, thermogram.fileData, {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json'
                })
            })
            //.get('assets/tmp-response.json', httpOptions)
            .subscribe(parsed => {
              // @ts-ignore
              thermogram.thermal = JSON.parse(parsed.thermal)
              // @ts-ignore
              thermogram.optical = parsed.optical
              thermogram.thermalFlat = sortNumbers(flatten(thermogram.thermal))

              this.thermograms.push(thermogram)

              this.thermogramIdx = 0
              this.showResults()
              this.headerBar.thermogramIdx = this.thermogramIdx
              this.headerBar.thermogramCount = this.thermograms.length
              this.headerBar.thermogramName = this.address
            });
      }
    }
    fileReader.addEventListener('load', processJSON)
    fileReader.readAsText(file)

    const endpoint = environment.base_url + "get-class-tree"
    this.http
        .get(endpoint, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        })
        .subscribe(classHierarchy => {
            this.classHierarchy = classHierarchy
        })
  }

  public readThermograms(files: Array<File>) {
    const self = this
    const thermograms: Array<Thermogram> = []
    for (const file of files) {
      const reader = new FileReader()
      const thermogram = new Thermogram(file)

      // Set onload call back to update new thermogram with base64 str of file
      // If last file, also query the AIPI
      reader.onload = function(e: Event) {
        thermogram.fileData = (reader.result as string)
        thermograms.push(thermogram)
        const last = thermograms.length === files.length
        if (last) {
          self.queryingAIPI = true
          self.queryAIPI(thermograms)
        }
      }

      reader.readAsDataURL(file)
    }

    // Set thermograms available
    this.thermograms = thermograms
  }

  /****************************************************************************/
  /*** Buttons ****************************************************************/
  /****************************************************************************/
  public handlerEnableAddingLeak() {
    this.renderer.handlerAdd()
  }

  public handlerCancelAddingLeak() {
    this.renderer.cancelManipulation()
  }

  public handlerToggleClassNames() {
    this.getCurrentThermogram().toggleClassNames()
  }

  /****************************************************************************/
  /*** AIPI contact ***********************************************************/
  /****************************************************************************/
  public queryAIPI(thermograms: Array<Thermogram>) {
    // Create json to send to server
    const json_struct = {}
    json_struct['version'] = 1
    json_struct['images'] = thermograms.map((t: Thermogram) => t.fileData)
    const httpOptions = { // Create HTTP options to do a correct HTTP request
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    const error = err => { // Callback in case query fails for whatever reason
      //console.log(err)
      alert('Analyse mislukt')
      this.queryingAIPI = false
      this.clearResults()
    }

    const processResults = rs => { // Calllback to show query results
      this.classHierarchy = rs['class_hierarchy']
      if (rs['images'].length !== json_struct['images'].length) {
        error("Expected the same amount of results as number of images sent, but they differ")
        return
      }

      for (let i = 0; i < rs['images'].length; i++) {
        const result = rs['images'][i]
        const therm = thermograms[i]
        therm.assign(result)
      }

      this.queryingAIPI = false
      this.setThermogram(0)
      this.showResults()
      this.changeDetector.detectChanges()
    }

    const endpoint = environment.base_url + "analyze"
    this.http
        .post(endpoint, JSON.stringify(json_struct), httpOptions)
        //.get('assets/tmp-response.json', httpOptions)
        .subscribe(processResults, error);
  }

  /****************************************************************************/
  /*** Result manipulations / renderings **************************************/
  /****************************************************************************/
  showResults() {
    if (!(this.thermograms.length > 0 && this.thermogramIdx !== undefined)) {
      //console.log("Nothing to show", this.thermograms.length, this.thermogramIdx)
      return
    }

    this.getCurrentThermogram().filterHeatLeaks()
    this.reporter.reportGeneratable = true
  }

  clearResults() {
    this.thermograms = []
    this.thermogramIdx = undefined
    this.classHierarchy = undefined
    this.reporter.reportGeneratable = false
  }

  handlerToggleShownImage() {
    this.getCurrentThermogram().toggleShownImage()
  }

  handlerResetLeaks() {
    this.getCurrentThermogram().resetLeaks()
  }

  handlerUndo() {
    this.renderer.undo()
  }

  public setThermogram(index: number) {
    this.thermogramIdx = index;
    this.headerBar.thermogramIdx = this.thermogramIdx+1
    this.headerBar.thermogramCount = this.thermograms.length;
    this.headerBar.thermogramName = this.getCurrentThermogram().file.name
  }

  private getCurrentThermogram(): Thermogram {
    return this.thermograms[this.thermogramIdx]
  }

  private beoordelingsgebiedPresent(): boolean {
    for (const hl of this.getCurrentThermogram().detectionsShown) {
        console.log("Chcloing!")
        if (hl.classCode === "Beoordelingsbied") return true;
    }
    return false;
  }

  /****************************************************************************/
  /*** Submitting leaks for the AI loop ***************************************/
  /****************************************************************************/
  handlerSubmitLeaks() {
    const saveObject = {
      "image": this.getCurrentThermogram().fileData,
      "results": {
        "External ID": this.getCurrentThermogram().file.name,
        "Warmteverlies": [],
        "Created By": "HeatBrain WebApp",
        "Project Name": "HeatBrain",
        "Created At": new Date()
      }
    }

    for (let leak of this.getCurrentThermogram().filterHeatLeaks_()) {
        saveObject.results.Warmteverlies.push({
            "Geometry": leak.mask,
            "Class Code:": [leak.getShortClassCode()]
        })
    }
    const httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };
    this.http.post(environment.base_url + "results", JSON.stringify(saveObject),
                   httpOptions)
        .subscribe(
          suc => {
              alert('Submit Successful.')
          },
          err => {
              alert('Submit Failed.')
          }
    );
  }
}
