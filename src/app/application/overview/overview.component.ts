import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SidebarService } from '../../services/sidebar.service';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit, OnDestroy {

    public sidebarVisible: boolean = true;
    public isResizing: boolean = false;

    public interval:any = {};

    constructor(private activatedRoute: ActivatedRoute, private sidebarService: SidebarService, private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {
    }

    ngOnDestroy(){
        if (this.interval){
            clearInterval(this.interval);
        }
    }

    toggleFullWidth() {
        this.isResizing = true;
        this.sidebarService.toggle();
        this.sidebarVisible = this.sidebarService.getStatus();
        let that = this;
        setTimeout(function () {
            that.isResizing = false;
            that.cdr.detectChanges();
        }, 200);
    }

}
