import { FormsModule } from '@angular/forms';
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { OverviewComponent } from "./overview/overview.component";
import { ApplicationComponent } from './application/application.component';
import { routing } from "./application.routing";
import { PagesModule } from "../pages/pages.module";
import { RouterModule } from "@angular/router";
import { LayoutModule } from "../layout/layout.module";
import { MapsModule } from "../maps/maps.module";
import { NgxFileDropModule } from 'ngx-file-drop';
import { SoboltLibModule } from "sobolt-lib";
import { HeatBrainComponent } from './heatbrain/heatbrain.component';
import { HeatBrainClassSelectorComponent } from './heatbrain-class-selector/heatbrain-class-selector.component';
import { ThermogramRendererComponent } from './thermogram-renderer/thermogram-renderer.component';
import { HeatBrainClassPipe } from './heatbrain-class-pipe';
import { HeatLeakDetailsComponent } from './heatleak-details/heatleak-details.component';
import { Ng2PanZoomModule } from "ng2-panzoom";

@NgModule({
  imports: [
    CommonModule,
    routing,
    LayoutModule,
    PagesModule,
    RouterModule,
    MapsModule,
    SoboltLibModule,
  	NgxFileDropModule,
  	FormsModule,
    Ng2PanZoomModule,
  ],
  declarations: [
    ApplicationComponent,
    OverviewComponent,
    HeatBrainComponent,
    ThermogramRendererComponent,
  	HeatBrainClassSelectorComponent,
  	HeatBrainClassPipe,
  	HeatLeakDetailsComponent,
  ]
})
export class ApplicationModule {}
