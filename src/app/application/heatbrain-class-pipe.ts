import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'formatclass' })
export class HeatBrainClassPipe implements PipeTransform {
    transform(class_name: string) {
        return class_name.replace(/_/g, ' ');
    }
}
