import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HeatLeak } from '../../interfaces/heatleak';

@Component({
  selector: 'heatbrain-class-selector',
  templateUrl: './heatbrain-class-selector.component.html',
  styleUrls: ['./heatbrain-class-selector.component.css']
})
export class HeatBrainClassSelectorComponent implements OnInit {
    @Input() heatLeak: HeatLeak
    @Input() classHierarchy
    @Output() onClassCodeChange = new EventEmitter<string>()

    depth0 = undefined
    depth1 = undefined
    depth2 = undefined
    depth3 = undefined
    depth4 = undefined
    depth5 = undefined

    key0: string = ""
    key1: string = ""
    key2: string = ""
    key3: string = ""
    key4: string = ""
    key5: string = ""
    key6: string = ""

    constructor() {
        this.depth0 = this.classHierarchy
    }

    ngOnInit() {
    }

    parseClassCode(code: string): string[] {
        /* Possible scenarios for the class code:
         *   1. short class code, no beoordelingsgebied: 112
         *   2. long class code, no beoordelingsgebied: 1:11:112
         *   3. long class code, beoordelingsgebied included in tree:
         *          a. Warmteverlies:1:11:112
         *          b. Beoordelingsgebied
         */
         const roiInTree = this.isRoiInTree()

         let keys = code.split(':')
         if (code.length > 1 && !(code.includes(':'))) {
            // short class code nested deeper than 1, construct exploded keys
            //
            // if only 1 layer deep, a short class code can not be differentiated
            // from a logn class code without a beoordelingsgebied, but in that
            // case it doesn't matter, since there is only 1 key
            keys = []
            for (let i = 0; i < code.length; i++) {
                let subcode = ""
                for (let j = 0; j <= i; j++) subcode += code[j];
                keys.push(subcode)
            }

            // If the backend _does_ support Beoordelingsgebied, include the
            // difference. Assumption: if it's a short class code it is always
            // a warmteverlies (heat leak)
            if (roiInTree) keys.unshift("Warmteverlies");
         }

         // otherwise it is the situation of long class code, which can be
         // handled similarly
         return keys
    }

    ngOnChanges() {
        this.depth0 = this.classHierarchy
        this.depth1 = undefined
        this.depth2 = undefined
        this.depth3 = undefined
        this.depth4 = undefined
        this.depth5 = undefined

        if (this.heatLeak.classCode && this.depth0) {
            const keys = this.parseClassCode(this.heatLeak.classCode)
            for (const key of keys) {
                // ROI_in_tree and ROI_root help distinguishing between what
                // kind of tree the backend sent the frontend.
                // In the beginning we only supported annotating heat leaks
                // ("Warmteverlies"). For that reason it wasn't important to
                // indicate that an annotation was a heat leak.
                // However, later we wanted to add support for annotating the
                // part of the house that is house that is being considered
                // ("Beoordelingsgebied").
                // The following strange construction reflects that organic
                // growth of requirements. It supports both the old and the new
                // situation.

                // ROI_in_tree detects whether we deal with the new situation
                // ROI_root detects whether the key currently looked at is
                //          a root node in the new situation
                const ROI_in_tree = this.isRoiInTree()
                const ROI_root = ROI_in_tree && (
                    key === "Warmteverlies" ||
                    key === "Beoordelingsgebied"
                ) && (
                    keys[0] === "Warmteverlies" ||
                    keys[0] === "Beoordelingsgebied"
                );

                const depth = ROI_in_tree ? ROI_root ? 1 : key.length + 1 : key.length;

                let h = undefined;
                if (depth === 1) h = this.depth0;
                if (depth === 2) h = this.depth1;
                if (depth === 3) h = this.depth2;
                if (depth === 4) h = this.depth3;
                if (depth === 5) h = this.depth4;
                if (depth === 6) h = this.depth4;
                this.update(h, depth, key)
            }
        }
    }

    update(hierarchy, depth, key) {
        if (depth <= 1) {
            this.depth1 = undefined;
        }
        if (depth <= 2) {
            this.depth2 = undefined;
            this.key1 = "";
        }
        if (depth <= 3) {
            this.depth3 = undefined;
            this.key2 = "";
        }
        if (depth <= 4) {
            this.depth4 = undefined;
            this.key3 = "";
        }
        if (depth <= 5) {
            this.key4 = "";
        }
        if (depth <= 6) {
            this.key5 = "";
        }

        if (hierarchy !== undefined && key !== undefined && key !== null && key in hierarchy) {
            const subclasses = hierarchy[key].hasOwnProperty("subclasses")
            if (depth == 1) {
                this.key0 = key
                if (subclasses)
                    this.depth1 = hierarchy[key].subclasses;
            }
            else if (depth == 2) {
                this.key1 = key
                if (subclasses)
                    this.depth2 = hierarchy[key].subclasses;
            }
            else if (depth == 3) {
                this.key2 = key
                if (subclasses)
                    this.depth3 = hierarchy[key].subclasses;
            }
            else if (depth == 4) {
                this.key3 = key
                if (subclasses)
                    this.depth4 = hierarchy[key].subclasses;
            }
            else if (depth == 5) {
                this.key4 = key
            }
            else if (depth == 6) {
                this.key5 = key
            }
        }

        let val = this.flattenedClassCode()
        this.onClassCodeChange.emit(val)
    }

    public flattenedClassCode() {
        let wholeCode = this.key0;
        if (this.key1) {
            wholeCode += ":" + this.key1;
            if (this.key2) {
                wholeCode += ":" + this.key2;
                if (this.key3) {
                    wholeCode += ":" + this.key3;
                    if (this.key4) {
                        wholeCode += ":" + this.key4;
                        if (this.key5) {
                            wholeCode += ":" + this.key5;
                            if (this.key6) {
                                wholeCode += ":" + this.key6;
                            }
                        }
                    }
                }
            }
        }

        return wholeCode
    }

    isRoiInTree(): boolean {
        return "Warmteverlies" in this.classHierarchy
    }

    /*public classNameFromCode(code: string) {
        // TODO deal with if the first part of the code is 'Warmteverlies'
        const nested_classes: string[] = []
        let subhierarchy = this.classHierarchy
        for (let i = 1; i < code.length; i++) {
            const class_h_idx = code.substring(0, i);

            subhierarchy = subhierarchy[class_h_idx]

            const subclass_name = subhierarchy["class_name"]
            subhierarchy = subhierarchy.get("subclasses", undefined)
            nested_classes.push(subclass_name)
        }

        return nested_classes[nested_classes.length - 1]
    }*/
}
