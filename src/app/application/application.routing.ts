import { Routes, RouterModule } from '@angular/router';
import { ApplicationComponent } from './application/application.component';
import { OverviewComponent } from './overview/overview.component';
import { HeatBrainComponent } from './heatbrain/heatbrain.component';
import { PageLeafletComponent } from '../maps/page-leaflet/page-leaflet.component';
import { PageProfileComponent } from '../pages/page-profile/page-profile.component';

const routes: Routes = [
    {
        path: '',
        component: ApplicationComponent,
        children: [
            { path: '', redirectTo:'heatbrain'},
            {
                path: 'heatbrain',
                component: HeatBrainComponent, data: { title: 'HeatBrain' }
            }

        ]
    },

];

export const routing = RouterModule.forChild(routes);
