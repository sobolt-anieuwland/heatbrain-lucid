import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Thermogram } from '../interfaces/thermogram';
import { ReportData } from '../interfaces/report-data';
import { ReportDataProvider, ReportDataConsumer } from '../interfaces/report-data-provider';

@Injectable({
  providedIn: 'root'
})
export class ReportService implements ReportDataConsumer {
  reportGeneratable: boolean = false
  reportDataProviders: Array<ReportDataProvider> = []

  constructor() { }

  public generateReport() {
    for (const provider of this.reportDataProviders) {
      const data = provider.provideReportData(this)
    }
  }

  public saveJson() {
    for (const provider of this.reportDataProviders) {
      const data = provider.provideReportData(this)
    }
  }

  public receiveReportData(data: ReportData) {
    this.generateReport_(data)
  }

  public addReportDataProvider(provider: ReportDataProvider) {
    this.reportDataProviders.push(provider)
  }

  /****************************************************************************/
  /*** Generating the report **************************************************/
  /****************************************************************************/
  generateReport_(data: ReportData) {
    const reportData = {
      "address": data.address,
      "city": data.city,
      "choose_date": data.dateEnabled,
      "date": data.dateEnabled ? data.dateAsDDMMYYYY() : "ddmmyyyy",
      "choose_knmi_station": data.knmiEnabled,
      "generation_date": new Date(),
      "length_report": data.lengthReport,
      "knmi_station": data.knmi,
      "palette": data.palette,
      "thermograms": [],
      "type_report": data.typeReport,
      "version": 1,
    }

    if (!environment.production) reportData['debug'] = !environment.production;

    // Combine the files and their corresponding leaks
    for (const thermogramObj of data.thermograms) {
      const leaks = []
      const thermogramDict = {
        "file": thermogramObj.fileData,
        "leaks": leaks,
        "min_percentile": thermogramObj.minPercentile,
        "max_percentile": thermogramObj.maxPercentile
      }
      reportData['thermograms'].push(thermogramDict)

      for (const leak of thermogramObj.filterHeatLeaks_()) {
        const firstClass = leak.getClassCode().split(':')[0]
        if (firstClass === "Beoordelingsgebied") {
            thermogramDict["beoordelingsgebied" ] = [leak.maskXs, leak.maskYs]
        } else {
            // Either firstClass === "Warmteverlies" or the tree does not
            // distinguish between the two because they aren't in there
            leaks.push({
              "geometry": [leak.maskXs, leak.maskYs],
              "class": leak.getShortClassCode()
            })
        }
      }
    }

    console.log(reportData)

    // Prepare the html form to submit to the server. Using the form instead
    // of Angular's HttpClient is a workaround to be able to do a POST
    // request whose result (a PDF) opens in a new window.
    const form = document.createElement("form")
    form.setAttribute("method", "POST")
    form.setAttribute("action", environment.base_url + "house-report")
    form.setAttribute("target", "_blank")

    const payload = document.createElement("input")
    payload.setAttribute("type", "hidden")
    payload.setAttribute("name", "payload")
    payload.setAttribute("value", JSON.stringify(reportData))
    form.appendChild(payload)

    document.body.appendChild(form)
    form.submit()
    form.remove()
  }
}
