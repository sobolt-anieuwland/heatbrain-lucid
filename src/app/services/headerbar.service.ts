import { Input, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderBarService {
  @Input() thermogramIdx: number = -1; // index of opened thermogram
  @Input() thermogramCount: number = 0; // amount of thermograms in this batch
  @Input() thermogramName: string = ""; // actual thermogram

  constructor() { }
}
