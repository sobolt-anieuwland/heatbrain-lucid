import { TestBed } from '@angular/core/testing';

import { HeaderbarService } from './headerbar.service';

describe('HeaderbarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HeaderbarService = TestBed.get(HeaderbarService);
    expect(service).toBeTruthy();
  });
});
