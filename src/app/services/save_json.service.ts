import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Thermogram } from '../interfaces/thermogram';
import { ReportData } from '../interfaces/report-data';
import { ReportDataProvider, ReportDataConsumer } from '../interfaces/report-data-provider';

@Injectable({
  providedIn: 'root'
})
export class JsonSavingService implements ReportDataConsumer {
  reportGeneratable: boolean = false
  reportDataProviders: Array<ReportDataProvider> = []

  constructor(private http: HttpClient) { }

  public saveJson() {
    for (const provider of this.reportDataProviders) {
      const data = provider.provideReportData(this)
    }
  }

  public receiveReportData(data: ReportData, callback: any = undefined) {
    this.generateReport_(data, callback)
  }

  public addReportDataProvider(provider: ReportDataProvider) {
    this.reportDataProviders.push(provider)
  }

  /****************************************************************************/
  /*** Generating the report **************************************************/
  /****************************************************************************/
  generateReport_(data: ReportData, callback) {
    // Returns true if successfully managed to start a post request, false if
    // not because the BOG was missing.
    // Note that true does not mean the post completed successfully.
    const reportData = {
      "address": data.address,
      "city": data.city,
      "choose_date": data.dateEnabled,
      "date": data.dateEnabled ? data.dateAsDDMMYYYY() : "ddmmyyyy",
      "choose_knmi_station": data.knmiEnabled,
      "generation_date": new Date(),
      "length_report": data.lengthReport,
      "knmi_station": data.knmi,
      "palette": data.palette,
      "thermograms": [],
      "type_report": data.typeReport,
      "version": 1,
    }

    if (!environment.production) reportData['debug'] = !environment.production;

    // Combine the files and their corresponding leaks
    for (const thermogramObj of data.thermograms) {
      const leaks = []
      const thermogramDict = {
        "file": thermogramObj.fileData,
        "leaks": leaks,
        "min_percentile": thermogramObj.minPercentile,
        "max_percentile": thermogramObj.maxPercentile
      }
      reportData['thermograms'].push(thermogramDict)

      for (const leak of thermogramObj.filterHeatLeaks_()) {
        const firstClass = leak.getClassCode().split(':')[0]
        if (firstClass === "Beoordelingsgebied") {
            thermogramDict["beoordelingsgebied"] = [leak.maskXs, leak.maskYs]
        } else {
            // Either firstClass === "Warmteverlies" or the tree does not
            // distinguish between the two because they aren't in there
            if (
                leak.getShortClassCode() === "Warmteverlies"
                || leak.getShortClassCode() === "" // none chosen
                || leak.getShortClassCode() === "0" // wv:onduidelijke/overige
            ) {
                alert("Lek aanwezig zonder geldige klasse, namelijk:" + leak.getShortClassCode())
                return
            }

            leaks.push({
              "geometry": [leak.maskXs, leak.maskYs],
              "class": leak.getShortClassCode()
            })
        }
      }

      // Prevent submission of thermograms without BOG
      if (!("beoordelingsgebied" in thermogramDict)) {
          alert("Geen beoordelingsgebied onder annotaties! JSON opslaan geannuleerd. Voeg een beoordelingsgebied toe om wel de annotaties op te kunnen slaan.")
          return
      }
    }

    // Submit!
    this.http.post(
        environment.base_url + "save-json",
        { "payload": reportData },
        {
          headers: new HttpHeaders({
              'Content-Type': 'application/json',
              "Accept": "*/*"
          }),
          responseType: "text"
        }
    ).subscribe(
        suc => { callback() },
        err => {
            alert("Storing JSON failed!")
        }
    )
  }
}
