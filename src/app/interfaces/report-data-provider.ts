import { ReportData } from './report-data';

export interface ReportDataProvider {
  provideReportData(consumer: ReportDataConsumer);
}

export interface ReportDataConsumer {
  receiveReportData(data: ReportData, callback: any);
}
