import { Thermogram } from './thermogram';

export class ReportData {
  public constructor(
    public address: string,
    public city: string,
    public date: string,
    public dateEnabled: boolean,
    public lengthReport: string,
    public knmi: number,
    public knmiEnabled: boolean,
    public palette: string,
    public thermograms: Array<Thermogram>,
    public typeReport: string
  ) { }

  public dateAsDDMMYYYY(): string {
    const date = new Date(this.date)

    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();

    return String(day).padStart(2, '0') +
           String(month+1).padStart(2, '0') +
           String(year);
  }
}
