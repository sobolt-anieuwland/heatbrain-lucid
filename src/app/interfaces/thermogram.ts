import { HeatLeak } from './heatleak'
import { flatten, sortNumbers, percentile } from '../functions/arrays'

export class Thermogram {
  // Detections as returned by the backend ('backup'), those the user finds
  // worthwhile ('known'), and those currently shown ('shown')
  detectionsBackup: Array<any> = [];
  detectionsKnown: Array<HeatLeak> = [];
  detectionsShown: Array<HeatLeak> = [];

  // Parameters that influence which detections are shown
  probMin: number = 0.0
  areaMax: number = 1.0
  minPercentile: number = 0.35
  maxPercentile: number = 0.99

  // File references and data
  fileData: string = ""; // base64 representation of data

  // Image data
  optical: string = ""; // base64 representation of the optical image in a FLIR
  thermal: Array<Array<number>> = []; // thermal data in a FLIR
  thermalFlat: Array<number> = []; // same data as thermal but flattened
  thermalImg: string = ""; // not entirely sure what, maybe prerendered FLIR?

  // Parameter deciding whether the thermal or optical should be shown
  thermalOrOptical: boolean = true // true shows thermal, false optical

  public constructor(
    public file: File
  ) { }

  public assign(result) {
    // Assigns result values gotten from the AI to this class
    //therm.filename = this.fileNames[i]
    this.detectionsBackup = result.data
    this.parseDetections()

    this.optical = result.optical
    this.thermal = JSON.parse(result.thermal_return)
    this.thermalFlat = flatten(this.thermal)
    this.thermalFlat = sortNumbers(this.thermalFlat)
    this.thermalImg = result.thermal
  }

  public parseDetections(raw: Array<any> | undefined = undefined) {
    if (raw === undefined)
      raw = this.detectionsBackup
    this.detectionsKnown = raw.map(hl => new HeatLeak(hl))
  }

  public getThermalDims(): Array<number> {
    const height = this.thermal.length
    const width = height > 0 ? this.thermal[0].length : 0
    return [width, height]
  }

  public filterHeatLeaks() {
    const toShow = this.filterHeatLeaks_()
    toShow.sort((a, b) => b.calcArea() - a.calcArea())
    this.detectionsShown = toShow
  }

  public filterHeatLeaks_() {
    const [width, height] = this.getThermalDims()
    const imgArea = width * height; // width * height

    return this.detectionsKnown.filter(hl => {
      return hl.probability >= this.probMin
    }).filter(hl => {
      return Math.sqrt(hl.calcArea() / imgArea) <= this.areaMax
    })
  }

  public remove(heatLeak: HeatLeak) {
    // Search in known detections for the one to remove
    let toRemoveIdx: number | undefined = undefined
    for (let idx = 0; idx < this.detectionsKnown.length; idx++) {
      const iterHeatLeak = this.detectionsKnown[idx]
      if (iterHeatLeak === heatLeak) toRemoveIdx = idx;
    }

    // Actually remove if found
    if (toRemoveIdx !== undefined) this.detectionsKnown.splice(toRemoveIdx, 1);
  }

  public toggleShownImage() {
    this.thermalOrOptical = !this.thermalOrOptical
  }

  public toggleClassNames() {
    for (const hl of this.detectionsKnown) hl.toggleClassName();
  }

  public hideClassNames() {
    for (const hl of this.detectionsKnown) hl.hideClassName();
  }

  public resetLeaks() {
    this.parseDetections()
    this.filterHeatLeaks()
  }

  public calcMinTemp(): number {
    return percentile(this.thermalFlat, this.minPercentile)
  }

  public calcMaxTemp(): number {
    //return percentile(this.thermalFlat, this.maxPercentile)
    return this.calcMinTemp() + 4.0; // specifically for Horus Groningen data
  }
}
