export class HeatLeak {
    // Model values
    probability: number = 1.0
    maskXs: number[]
    maskYs: number[]
    mask: string
    classCode: string = ""
    className: string = ""

    // ViewModel values
    polygonPoints: string

    // For the polygon-canvas
    style: string = "sobolt-green"
    textBoxPosition: number[] = [0, 0]
    textBoxPositionHide: number[] = [0, 0]
    type = {
        name: "Warmteverlies"
    }

    constructor(values) {
        if (!values) return;

        this.classCode = values['class_code'];
        this.probability = values['probability'] || this.probability;
        this.mask = values['mask'];
        const maskCoords = this.parseMask(this.mask);
        this.maskXs = maskCoords[0]
        this.maskYs = maskCoords[1]
        this.recalculatePolygonCoords()
        this.updateTextBoxPosition()
    }

    // 'Parses' a WKT polygon string of the form POLYGON (( ... )) to extract
    // and convert to int the coordinates between the parentheses. It is not a
    // smart function. It parses by cutting of the first 10 and last 2 characters.
    // Returns: a list of X coordinates, a list of Y coordinates, and a string
    //          based on those values directly usable in an svg:polygon element.
    public parseMask(wktstr: string): [number[], number[]] {
        // String of the format POLYGON (( .. ))
        const pgn = wktstr.trim().slice(10, -2).trim()
        const coords_str = pgn.split(", ")

        const xs: number[] = []
        const ys: number[] = []

        coords_str.forEach(function(coord_str) {
            const coord_str_parts = coord_str.split(' ')
            if(!coord_str_parts[0]){
                return;
            }
            const x = parseInt(coord_str_parts[0])
            const y = parseInt(coord_str_parts[1])

            xs.push(x)
            ys.push(y)
        });

        return [xs, ys]
    }

    public recalculatePolygonCoords() {
        var points = ""

        for (let idx = 0; idx < this.maskXs.length; idx++) {
            const x = this.maskXs[idx]
            const y = this.maskYs[idx]
            points += " " + x.toString() + "," + y.toString()
        }
        this.polygonPoints = points
    }

    public calcArea(): number {
        let area = 0;
        let j = this.maskXs.length - 1;

        for (let i = 0; i < this.maskXs.length; i++) {
            area = area +  (this.maskXs[j]+this.maskXs[i]) * (this.maskYs[j]-this.maskYs[i]);
            j = i;
        }
        return Math.abs(area / 2);
    }

    public probabilityPercentage(): number {
      return Math.floor(this.probability*100)
    }

    public setClass(code: string, name: string | undefined = undefined) {
        this.classCode = code
        if (name) this.className = name;
        else this.className = this.classCode;

        this.type.name = this.className
        console.log(this.classCode)
        console.log(this.className)
    }

    public getClassCode() {
        return this.classCode
    }

    public getShortClassCode() {
        const partialCodes = this.classCode.split(':')
        return partialCodes[partialCodes.length-1]
    }

    public updateClassCode(classCode: string) {
        this.classCode = classCode
    }

    public toggleClassName() {
        if (this.textBoxPosition === undefined) this.showClassName();
        else this.hideClassName();
    }

    public hideClassName() {
        this.textBoxPosition = undefined;
    }

    public showClassName() {
        this.textBoxPosition = this.textBoxPositionHide;
    }

    public updateTextBoxPosition(show: boolean = true) {
        this.textBoxPositionHide = [this.maskXs[0], this.maskYs[0]]
        if (show) this.showClassName();
        else this.hideClassName();
    }
}
