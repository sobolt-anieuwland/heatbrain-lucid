import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { AuthenticateService } from "sobolt-lib";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { setUserDetails } from "../../store/actions/authentication.actions";
import { Store, select } from "@ngrx/store";
import { User } from "../../interfaces/auth";

@Component({
  selector: "app-page-login",
  templateUrl: "./page-login.component.html",
  providers: [AuthenticateService],
  styleUrls: ["./page-login.component.css"]
})
export class PageLoginComponent implements OnInit {
  user$: Observable<User>;

  constructor(
    private store: Store<{ user: User }>,
    private router: Router,
    private authenticateService: AuthenticateService

  ) {
    this.user$ = store.pipe(select("user"));
  }

  ngOnInit() {}

  onDemoClick() {
    this.router.navigate(["/app/snelstart"]);
  }

  onSubmit(form: NgForm) {
    this.login(form.value.email, form.value.password);
  }

  login(email, password) {
    console.log(environment.base_url)
    const loginUrl = `${environment.base_url}users/${email}`;

    this.authenticateService
      .login(email, password, loginUrl)
      // .subscribe((permissions: string[]) => {
      .subscribe((permissions: any) => {
        this.store.dispatch(
          setUserDetails({
            email: email,
            password: password,
            permissions: permissions
          })
        ),
        this.router.navigate(["/application/"])
      });
  }
}
