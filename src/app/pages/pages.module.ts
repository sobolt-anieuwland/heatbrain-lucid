import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageProfileComponent } from './page-profile/page-profile.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
	declarations: [
		PageProfileComponent,
	],
	imports: [
		CommonModule,
		NgbModule,
		RouterModule,
	],
	exports: []
})
export class PagesModule { }
