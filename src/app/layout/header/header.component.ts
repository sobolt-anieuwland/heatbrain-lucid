import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { ThemeService } from '../../services/theme.service';
import { ReportService } from '../../services/report.service';
import { JsonSavingService } from '../../services/save_json.service';
import { HeaderBarService } from '../../services/headerbar.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css'],
	providers: [NgbDropdownConfig]
})
export class HeaderComponent implements OnInit {
	// Properties
	@Input() showNotifMenu: boolean = false;
  @Input() showToggleMenu: boolean = false;
  @Input() darkClass: string = "";
  reportGeneratable: boolean = false;

	constructor(
	  private config: NgbDropdownConfig,
	  private themeService: ThemeService,
	  private reporter: ReportService,
	  private jsonSaver: JsonSavingService,
	  private headerBar: HeaderBarService,
  ) {
		config.placement = 'bottom-right';
	}

	ngOnInit() {
	}

	toggleSideMenu(){
		this.themeService.showHideMenu();
	}

  handlerGenerateReport() {
    this.reporter.generateReport()
  }

  handlerSaveReportJson() {
    this.jsonSaver.saveJson()
  }
}
