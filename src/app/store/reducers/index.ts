import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from "@ngrx/store";
import { environment } from "../../../environments/environment";
import { authenticationReducer } from "./authentication.reducer";
import { User } from "../../interfaces/auth";

export interface State {
  user: User;
}

export const reducers: ActionReducerMap<State> = {
  user: authenticationReducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];
