import { Action, createReducer, on } from "@ngrx/store";
import * as AuthenticationActions from "../actions/authentication.actions";
import { User } from "../../interfaces/auth";

function retrieveState() {
  const savedState = sessionStorage.getItem("state");

  if (savedState === null) {
    return {
      email: "",
      password: "",
      permissions: [""]
    };
  } else {
    const stateObject = JSON.parse(savedState);
    return stateObject.user;
  }
}

export const initialState: User = retrieveState();

const authenticationReducer_ = createReducer(
  initialState,
  on(AuthenticationActions.setUserDetails, (state, { email, password, permissions }) => ({
    ...state,
    email: email,
    password: password,
    permissions: permissions
  })),
  on(AuthenticationActions.logout, (state) => {
    sessionStorage.clear();
    return initialState;
  })
);
export function authenticationReducer(user: User | undefined, action: Action) {
    return authenticationReducer_(user, action)
}
