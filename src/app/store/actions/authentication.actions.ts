import { createAction, props } from "@ngrx/store";
// import { IQbsInspection, IQbsObject } from "../../interfaces/api";

export const setUserDetails = createAction(
  "[Authentication] Login",
  props<{ email: string; password: string; permissions: string[] }>()
);
export const logout = createAction("[Authentication] Logout");
